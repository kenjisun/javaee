package ImgLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;



/**
 * Servlet implementation class ImgLoad
 */
@WebServlet("/ImgLoad")
public class ImgLoad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImgLoad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String filename_in = request.getParameter("filename");
		ServletContext cntx= getServletContext();
		
		
		//HLWIN7/nicePlace/kkk.txt
		String user = "kenji:kenji";
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(user);
        String path = "smb://"+filename_in;
        SmbFile sFile = new SmbFile(path, auth);
        SmbFileInputStream sfis = new SmbFileInputStream(sFile);
        //sfos.write("Muneeb Ahmad".getBytes());
        
        
        
		//\\Users\\kenji\\EFFCOMP
//		String sep = System.getProperty("file.separator");
//		String remotePath = "\\\\HLWIN7\\nicePlace";
//		File f=new File("abc.txt"); //Takes the default path, else, you can specify the required path
//        if(f.exists())
//        {
//            f.delete();
//        }
//        f.createNewFile();
//		FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());
//		System.out.println("destn= "+destn.toString());
//		System.out.println("remotePath= "+remotePath);
//		UserAuthenticator auth=new StaticUserAuthenticator("192.168.0.106", "Kenji Chen", "kenji");
//        FileSystemOptions opts=new FileSystemOptions();
//        DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
//        FileObject fo=VFS.getManager().resolveFile(remotePath,opts);
//        destn.copyFrom(fo.getChild("kkk.txt"),Selectors.SELECT_SELF);
//        destn.close();
        
	      //String filename = cntx.getRealPath("WEB-INF/"+filename_in);
	      String mime = cntx.getMimeType(path);
	      if (mime == null) {
	    	  response.getWriter().println("File not found");
	    	  response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	        return;
	      }

	      response.setContentType(mime);
	      //File file = new File(filename_in);
	      
	      response.setContentLength((int)sFile.length());

	      //FileInputStream in = new FileInputStream(file);
	      OutputStream out = response.getOutputStream();

	      // Copy the contents of the file to the output stream
	       byte[] buf = new byte[1024];
	       int count = 0;
	       while ((count = sfis.read(buf)) >= 0) {
	         out.write(buf, 0, count);
	      }
	    out.close();
	    sfis.close();
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
